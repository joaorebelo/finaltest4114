//
//  MakeReservationViewController.swift
//  InClassExercisesStarter
//
//  Created by parrot on 2018-11-22.
//  Copyright © 2018 room1. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Firebase
import Alamofire
import SwiftyJSON

class MakeReservationViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var seatsTextField: UITextField!
    @IBOutlet weak var errorlbl: UILabel!
    
    // Mark: Firestore variables
    var db:Firestore!
    
    // MARK: Default Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        db = Firestore.firestore()
        
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func buttonPressed(_ sender: Any) {
        print("pressed the button")
        
        
        
        var isRestaurant = "false";
        let days:[String] = ["Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"]

        if days.contains(dayTextField.text!as String) {
            print("yes")
        
        let url = "https://opentable.herokuapp.com/api/restaurants?city=Toronto&per_page=5"
        Alamofire.request(url, method: .get, parameters: nil).responseJSON {
            (response) in
            
            // -- put your code below this line
            
            if (response.result.isSuccess) {
                do {
                    let json = try JSON(data:response.data!)
                    print(json)
                    
                    
                    let restaurantList = json["restaurants"].array!
                    
                    for x in restaurantList {
                        print(x["name"].string!)
                        print(">"+self.nameTextField.text!)
                        var n1 = x["name"].string!
                        var n2 = self.nameTextField.text! as String
                        if n1 == n2 {
                            isRestaurant = "true";
                            print("aqui")
                        }
                    }
                    print("is true: \(isRestaurant)")
                    if isRestaurant == "true"{
                        
                        //"username": Auth.auth().currentUser!,
                        self.db.collection("reservations").document().setData([
                            "username": Auth.auth().currentUser!.email!,
                            "restaurant": self.nameTextField.text!,
                            "day": self.dayTextField.text!,
                            "numSeats": self.seatsTextField.text!
                        ]) { err in
                            if let err = err {
                                self.errorlbl.text="Error writing document: \(err)"
                            } else {
                                self.errorlbl.text="Reservation Added"
                            }
                        }
                    }else{
                        self.errorlbl.text="Incorrect Restaurant Name"
                    }
                }
                catch {
                    print ("Error while parsing JSON response")
                }
            }
        }
            
        }else{
            self.errorlbl.text="Incorrect day Name"
        }
        
        
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
